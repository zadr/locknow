//
//  AppDelegate.swift
//  LockNow
//
//  Created by Izzy Fraimow on 5/8/15.
//  Copyright (c) 2015 Izzy Fraimow. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    private let statusItem = NSStatusBar.systemStatusBar().statusItemWithLength(-2)
    private var statusItemMenu: NSMenu {
        get {
            let menu = NSMenu()

            if let lockAgentURL = lockAgentURL {
                if let agentItem = menu.addItemWithTitle("Start at login", action: "updateLaunchAgent:", keyEquivalent: "l") {
                    if NSFileManager.defaultManager().fileExistsAtPath(lockAgentURL.path!) {
                        agentItem.state = Int(NSOnState)
                    } else {
                        agentItem.state = Int(NSOffState)
                    }
                }
            }

            menu.addItemWithTitle("Quit", action: "terminate:", keyEquivalent: "q")

            return menu
        }
    }

    private var lockAgentURL: NSURL? {
		get {
			if let libraryPath = NSSearchPathForDirectoriesInDomains(.LibraryDirectory, .UserDomainMask, true).first {
				let agentsDirectory = NSURL(fileURLWithPath: libraryPath).URLByAppendingPathComponent("LaunchAgents", isDirectory: true)
				return agentsDirectory.URLByAppendingPathComponent("com.anathemacalculus.locknow.launchagent.plist")
			}

			return nil
		}
    }

    func applicationDidFinishLaunching(aNotification: NSNotification) {
        if let button = statusItem.button {
            button.image = NSImage(named: "lock")
            button.action = "lockScreen:"
        }
    }
    
    @objc private func lockScreen(sender: AnyObject) {
        if let event = NSApplication.sharedApplication().currentEvent {
            if event.modifierFlags.intersect(.AlternateKeyMask) != [] {
                statusItem.popUpStatusItemMenu(statusItemMenu)
                return
            }

            if event.modifierFlags.intersect(.CommandKeyMask) != [] {
                NSApplication.sharedApplication().terminate(self)
                return
            }
        }
        
        SACLockScreenImmediate()
    }

    @objc private func updateLaunchAgent(sender: AnyObject) {
		guard let lockAgentURL = lockAgentURL else { return }
		guard let agentDirectoryURL = lockAgentURL.URLByDeletingLastPathComponent else { return }
		guard let providedLockAgentURL = NSBundle.mainBundle().URLForResource("com.anathemacalculus.locknow.launchagent", withExtension: "plist") else { return }

		if NSFileManager.defaultManager().fileExistsAtPath(lockAgentURL.path!) {
			let _ = try? NSFileManager.defaultManager().removeItemAtURL(lockAgentURL)
		} else {
			if !NSFileManager.defaultManager().fileExistsAtPath(agentDirectoryURL.path!, isDirectory: nil) {
				let _ = try? NSFileManager.defaultManager().createDirectoryAtURL(agentDirectoryURL, withIntermediateDirectories: true, attributes: nil)
			}

			let _ = try? NSFileManager.defaultManager().copyItemAtURL(providedLockAgentURL, toURL: lockAgentURL)
		}
	}
}

